//
//  LoginViewController.h
//  Band
//
//  Created by brst on 07/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
{
    UITapGestureRecognizer *tapRecognizer;
}
@property (weak, nonatomic) IBOutlet UITextField *email_textField;
- (IBAction)loginAction:(UIButton *)sender;
- (IBAction)createAccountAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *error_Label;

@end
