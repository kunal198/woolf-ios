//
//  RegionsTableCell.h
//  Band
//
//  Created by brst on 19/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegionsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *region_name;
@property (weak, nonatomic) IBOutlet UILabel *otherdata_label;
@property (weak, nonatomic) IBOutlet UISwitch *switch_cell;

@end
