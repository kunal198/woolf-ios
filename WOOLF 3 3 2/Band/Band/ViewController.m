//
//  ViewController.m
//  Band
//
//  Created by brst on 20/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import "ViewController.h"
#import "SettingsViewController.h"

@interface ViewController ()
{
    float pageViewControllerOffsetX;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //NSNumber *fileSize = [NSNumber numberWithInt:11018947];
    long long fileSize = 11018000;
    NSString *displayFileSize = [NSByteCountFormatter stringFromByteCount:fileSize
                                                               countStyle:NSByteCountFormatterCountStyleFile];
    NSLog(@"Display file size: %@", displayFileSize);
    pageViewControllerOffsetX = 0;
    // Do any additional setup after loading the view, typically from a nib.
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    //self.viewControllers = [NSArray arrayWithObjects:[self viewControllerAtIndex:0],[self viewControllerAtIndex:1],[self viewControllerAtIndex:2], nil];
    NSArray *viewControllers = @[startingViewController];

    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollToNext) name:@"Peripheralconnect" object:nil];

    [self.view bringSubviewToFront:_settings_Button];
    /*for (UIView *view in _pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView *)view).delegate = self;
            break;
        }
    }*/
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    /*(NSLog(@"%f",scrollView.contentOffset.x);
    
    NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"] == 2 && [idofconnected  isEqual: @""] && pageViewControllerOffsetX < scrollView.contentOffset.x) {
     
        scrollView.contentOffset = CGPointMake(self.view.frame.size.width-1, 0);
        
        //[self showViewControllerAtIndex:1 animated:NO];

        pageViewControllerOffsetX = scrollView.contentOffset.x;

    }*/
}

/*- (void)showViewControllerAtIndex:(NSUInteger)index animated:(BOOL)animated {
    self.presentationPageIndex = index;
    [self.pageViewController setViewControllers:@[self.viewControllers[index]] direction:UIPageViewControllerNavigationDirectionForward animated:animated completion:nil];
}*/

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    //NSLog(@"before.......");
    
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        
        //[[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"PageVisible"];

        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    //NSLog(@"after.......");

    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    //NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];

    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    //if (index == 3 || (index == 2 && [idofconnected  isEqual: @""])) {
    if (index == 3){
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers
{
    //NSLog(@"will........");
}
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    //NSLog(@"finish.........");
}
- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    //NSLog(@"index.......%lu",(unsigned long)index);

    //NSLog(@"index %i",index);
    if ((index >= 3)) {
        return nil;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:index forKey:@"PageVisible"];
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    
    /*if ([[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"] == 2 && [idofconnected  isEqual: @""]) {
        
        pageContentViewController.pageIndex = index;
        
    }
    else
    {*/
        pageContentViewController.pageIndex = index;
    //}
    
    return pageContentViewController;
}

- (void)scrollToNext
{
    currentIndex = 1;
    UIViewController *nextController = [self viewControllerAtIndex:currentIndex];
    if (nextController) {
        NSArray *viewControllers = @[nextController];
        // This changes the View Controller, but PageControl doesn't update
        [self.pageViewController setViewControllers:viewControllers
                       direction:UIPageViewControllerNavigationDirectionForward
                        animated:NO
                      completion:nil];
        //Nothing happens!
//        [self.pageControl setCurrentPage:currentIndex];
        
        
        //Error: _installAppearanceSwizzlesForSetter: Not a setter!
//        [self.pageControl updateCurrentPageDisplay];
    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    /*if (currentIndex)
    {
        return currentIndex;
    }*/
    
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)settingsAction:(UIButton *)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    [self.navigationController pushViewController:add animated:true];

}


@end
