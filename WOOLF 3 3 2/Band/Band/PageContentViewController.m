//
//  PageContentViewController.m
//  Band
//
//  Created by brst on 20/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import "PageContentViewController.h"
#import "Pair_collectioncell.h"
#import "MBBatteryInfoModel.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self Pagesteup];
    // Do any additional setup after loading the view.
}

#pragma MArk :- Notification Methods Defination:-
-(void)Pagesteup
{
    NSLog(@"self.pageIndex %lu",(unsigned long)self.pageIndex);
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for (UIView *subview in self.view.subviews)
    {
        subview.hidden = YES;
    }
    UIView *view = (UIView *)[self.view viewWithTag:101 + self.pageIndex];
    self.devicesArray = [[NSMutableArray alloc]init];
    device_selectedindex = -1;
    NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];
    
    NSLog(@"fromThirdView %hhd",[[NSUserDefaults standardUserDefaults] boolForKey:@"fromthirdscreen"]);
    if (self.pageIndex == 1)
    {
        [self deleteDevices];
        
    } /*else if (self.pageIndex == 0 && ![idofconnected isEqualToString:@""] && [[NSUserDefaults standardUserDefaults] boolForKey:@"fromthirdscreen"] == NO)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromthirdscreen"];
        
    } else if (self.pageIndex == 0 && ![idofconnected isEqualToString:@""] && [[NSUserDefaults standardUserDefaults] boolForKey:@"fromthirdscreen"] == YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromthirdscreen"];
        [self deleteDevices];
    }*/
    
    if (self.pageIndex == 2)
    {
        
        self.characteristicDictionary = appDel.characteristicDictionary1;
        self.discoveredPeripheral = appDel.connectingperipheral1;
        NSLog(@"%@",self.characteristicDictionary);
        NSLog(@"%@",self.discoveredPeripheral);
        [self readbatery];
    }
    //  BatteryLevelFound
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showbattery:) name:@"BatteryLevelFound" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addperipheraltoArray:) name:@"peripheralFound" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Updateconnectstatus:) name:@"Peripheralconnect" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedisconnectstatus:) name:@"Peripheraldisconnect" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectPeripheral) name:@"disconnectPeripheral" object:_connectingperipheral];

    view.hidden = NO;
}

-(void)checkPreviousdevices
{
    NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if (![idofconnected  isEqual: @""])
    {
        self.devicesArray = appDel.devicesFound.mutableCopy;
        
        for (CBPeripheral *peripheral in self.devicesArray)
        {
            NSUUID *idstring = peripheral.identifier;
            NSString *idstring1 = [idstring UUIDString];
            if ([idstring1 isEqualToString:idofconnected])
            {
                device_selectedindex = [self.devicesArray indexOfObject:peripheral];
                self.connected = true;
                break;
            }
        }
        
        [_Pair_status_btn setTitle:@"Paired" forState:UIControlStateNormal];

        [self.collection_view reloadData];

    }
}
-(void)deleteDevices
{
    NSLog(@"%@",self.devicesArray);
    
    device_selectedindex = -1 ;
    [_Pair_status_btn setTitle:@"Pair" forState:UIControlStateNormal];
    [self.devicesArray removeAllObjects];
    [self.collection_view reloadData];
}

-(void)addperipheraltoArray:(NSNotification *) notification
{
    
    CBPeripheral *peripg = notification.object;
    if ([_devicesArray count] >0)
    {
        BOOL sameDeviceExists = NO;
        
        for (int i =0 ;i < [_devicesArray count];i++)
        {
            CBPeripheral *peri = [_devicesArray objectAtIndex:i];
            if (![peri.identifier isEqual:peripg.identifier])
            {
                sameDeviceExists = NO;
            }
            else
            {
                sameDeviceExists = YES;
                [self.devicesArray replaceObjectAtIndex:i withObject:peripg];

                break;
            }
        }
        
        if (sameDeviceExists == NO)
        {
            [self.devicesArray addObject:peripg];
            [_collection_view reloadData];
        }
    }
    else
    {
        [self.devicesArray addObject:peripg];
 
        [_collection_view reloadData];
    }
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDel.devicesFound = self.devicesArray;
}
-(void)Updateconnectstatus:(NSNotification *) notification
{
    //NSLog(@"%@",self.characteristicDictionary);
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    CBPeripheral *connectedperipheral = notification.object;
    appDelegate.connectingperipheral1 = connectedperipheral;
    NSUUID *idstring = connectedperipheral.identifier;
    NSString *idstring1 = [idstring UUIDString];
    [[NSUserDefaults standardUserDefaults] setValue:idstring1 forKey:@"connectedperipheralid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super alert:NGMiBandImmediateAlertLevelCustom2];
    
    [_Pair_status_btn setTitle:@"Paired" forState:UIControlStateNormal];
    
    [self.collection_view reloadData];
    int64_t delay = 6.0;
    int64_t delayvibrate = 1.0;// In seconds
    //NSLog(@"self.discoveredPeripheral %@",self.discoveredPeripheral);
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delayvibrate * NSEC_PER_SEC);
    dispatch_time_t time1 = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
    dispatch_after(time, dispatch_get_main_queue(), ^(void)
                   {
                       [super alert:NGMiBandImmediateAlertLevelCustom2];
                   });
    
    dispatch_after(time1, dispatch_get_main_queue(), ^(void)
                   {
                       [self readbatery];
                   });
    }

-(void)updatedisconnectstatus:(NSNotification *) notification
{
    device_selectedindex = -1;
    [self.collection_view reloadData];
    [_Pair_status_btn setEnabled:false];
    _connectingperipheral = nil;
    [_Pair_status_btn setTitle:@"Pair" forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"connectedperipheralid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)showbattery:(NSNotification *) notification
{
    MBBatteryInfoModel *batteryInfo = [[MBBatteryInfoModel alloc] initWithData:notification.object];
    
    if(self.characteristicDictionary != nil)
    {
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDel.characteristicDictionary1 = self.characteristicDictionary;
    }
    NSLog(@"Battery Level %lu",(unsigned long)batteryInfo.level);
    _Battery_Level_label .text =[NSString stringWithFormat:@"Battery Level %lu %@",(unsigned long)batteryInfo.level,@"%"];
  
}
-(void)connecperipheral :(CBPeripheral*) peripheral
{
    self.discoveredPeripheral = peripheral;
    [self peripheral_connection:peripheral];
}

#pragma Mark :- supporting Method Defination
- (void)refreshDevices
{
    device_selectedindex = -1 ;
    [self scanForPeriphereal:self.centralManager ];
}
- (void)vibrate:(short)times duration:(double)duration pause:(double)pause{
    if(times > 0) {
        [super alert:NGMiBandImmediateAlertLevelHigh];
        // Delay execution of my block for 10 seconds.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [super alert:NGMiBandImmediateAlertLevelNOAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, pause * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self vibrate:times-1 duration:duration pause:pause];
            });
        });
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark :- collectionview Delegate and Datasource
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"devicesArray.count %lu",(unsigned long)self.devicesArray.count);
    return [self.devicesArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Pair_collectioncell *myCell = [collectionView
                                   dequeueReusableCellWithReuseIdentifier:@"cell"
                                   forIndexPath:indexPath];
    
    
    if (device_selectedindex == indexPath.row)
        if (self.connected == true)
        {
            myCell.pair_selectimg.hidden = false;
            myCell.Activityindicator.hidden = true;
            [myCell.Activityindicator stopAnimating];
            
        }
        else
        {
            myCell.Activityindicator.hidden = false;
            [myCell.Activityindicator startAnimating];
            
        }
    
        else
        {
            myCell.pair_selectimg.hidden = true;
            myCell.Activityindicator.hidden = true;
        }
    myCell.Pair_img.alpha = 0.5;
    
    return myCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
#define kCellsPerRow 3
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
    CGFloat availableWidthForCells = CGRectGetWidth(collectionView.frame) - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (kCellsPerRow - 1);
    CGFloat cellWidth = availableWidthForCells / kCellsPerRow;
    flowLayout.itemSize = CGSizeMake(cellWidth,cellWidth  );
    return flowLayout.itemSize ;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger currentindex = indexPath.row;
    
    if (currentindex == device_selectedindex )
    {
        device_selectedindex = indexPath.row;
    }
    else
    {
        if  (_connectingperipheral != nil)
        {
            [self peripheral_Disconnection:_connectingperipheral];
        }
        
        device_selectedindex = indexPath.row;
        _connectingperipheral = [self.devicesArray objectAtIndex:indexPath.row];
        
        int64_t delay = 2.0;// In seconds
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
        
        dispatch_after(time, dispatch_get_main_queue(), ^(void){
            [self connecperipheral:self.connectingperipheral];
        });
    }
    [_Pair_status_btn setEnabled:true];
    [collectionView reloadData];
    return;
}

-(void)vibrateOnly
{
    [super alert:NGMiBandImmediateAlertLevelCustom2];
}

-(void) disconnectPeripheral
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    //NSLog(@"%@",appDelegate.connectingperipheral1);
    //NSLog(@"%@",appDelegate.connectingperipheral1);
    
    [self peripheral_Disconnection:appDelegate.connectingperipheral1];
}

#pragma  mark :-  Button Action

- (IBAction)Pair:(id)sender
{
    NSLog(@"%hhu",self.connected);
}
- (IBAction)GOaction:(UIButton *)sender {
    
    NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];
    NSLog(@"%@",idofconnected);
    
    if (![idofconnected  isEqual: @""])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *add =
        [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        [self.navigationController pushViewController:add animated:true];
    }
}
@end
