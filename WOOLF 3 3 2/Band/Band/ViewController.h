//
//  ViewController.h
//  Band
//
//  Created by brst on 20/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PageContentViewController.h"

@interface ViewController : UIViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate, UIScrollViewDelegate>
{
    NSInteger currentIndex;
}
@property (weak, nonatomic) IBOutlet UIButton *settings_Button;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property int presentationPageIndex;
@property NSUInteger pageIndex;
@property (nonatomic, strong) NSArray *viewControllers;




@end

