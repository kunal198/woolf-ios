//
//  AppDelegate.h
//  Band
//
//  Created by brst on 20/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CBPeripheral* connectingperipheral1;
@property (strong, nonatomic) NSArray *devicesFound;
@property (nonatomic, strong) NSMutableDictionary *characteristicDictionary1;
@property (nonatomic, strong) NSHTTPCookie *cookieslogin;

@end

