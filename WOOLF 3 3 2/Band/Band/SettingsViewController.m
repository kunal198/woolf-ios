//
//  SettingsViewController.m
//  Band
//
//  Created by brst on 16/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import "SettingsViewController.h"
#import "RegionsTableCell.h"
#import "AppDelegate.h"

@interface SettingsViewController ()<NSURLSessionDownloadDelegate>{
    NSURLSessionDownloadTask *download;
    
}

@property (nonatomic, strong)NSURLSession *backgroundSession;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataArray = [[NSMutableArray alloc] init];
    switchState = [[NSMutableArray alloc] initWithObjects:@"off",@"off", nil];
    
    self.regions_Table.separatorColor = [UIColor clearColor];
    
    // 1
    NSURLSessionConfiguration *backgroundConfigurationObject = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"myBackgroundSessionIdentifier"];
    
    // 2
    self.backgroundSession = [NSURLSession sessionWithConfiguration:backgroundConfigurationObject delegate:self delegateQueue:[NSOperationQueue mainQueue]];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSDictionary *cookiesdict = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookies"];
    if(![cookiesdict  isEqual: @""] && cookiesdict != nil)
    {
        [self getRegions];
    } else {
        NSLog(@"create");
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Message"
                                     message:@"Please Login to See Regions List"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* okButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                    }];
        
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (void)viewDidLayoutSubviews
{
   // [_scroll_View addSubview:_main_View];
    if ([[UIScreen mainScreen] bounds].size.height <= 568)
    {
        //iphone 5
        _scroll_View.contentSize = CGSizeMake(_main_View.frame.size.width, _main_View.frame.size.height+180);
    }
    else if([[UIScreen mainScreen] bounds].size.height == 736)
    {
        _scroll_View.contentSize = CGSizeMake(_main_View.frame.size.width, _main_View.frame.size.height+100);
    } else {
    
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) getRegionData:(NSString*)urlwithid
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //NSString *baseURL = [NSString stringWithFormat:@"http://faang.woolf.bike/v1/region/DE"];
    /*NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSURL *url = [NSURL URLWithString:[baseURL stringByAddingPercentEncodingWithAllowedCharacters:set]];*/
    [request setURL:[NSURL URLWithString:urlwithid]];
    [request setHTTPMethod:@"GET"];
    NSDictionary *cookiesdict = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookies"];
    [request setAllHTTPHeaderFields:cookiesdict];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"accept"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSDictionary *jsondict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        NSLog(@"requestReply: %@", [jsondict valueForKey:@"features"]);
        NSArray *dataarray = [jsondict valueForKey:@"features"];
        NSLog(@"%@",[dataarray[0] valueForKey:@"properties"]);
        NSDictionary *propertiesdict = [dataarray[0] valueForKey:@"properties"];
        NSLog(@"%@",[propertiesdict valueForKey:@"drawModelURI"]);
    }] resume];
}
-(void) getRegions
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://faang.woolf.bike/v1/regions"]];
    [request setHTTPMethod:@"GET"];
    NSDictionary *cookiesdict = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookies"];
    [request setAllHTTPHeaderFields:cookiesdict];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"accept"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSDictionary *jsondict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        NSLog(@"requestReply: %@", jsondict);
        NSArray *dataarray = [jsondict valueForKey:@"features"];
        dataArray = dataarray.mutableCopy;
        NSLog(@"%@",dataArray);
        NSLog(@"%lu",(unsigned long)dataArray.count);
        NSLog(@"%@",[dataarray[0] valueForKey:@"properties"]);
        NSLog(@"%@",[dataarray[0] valueForKey:@"id"]);
        dispatch_async(dispatch_get_main_queue(), ^{
        [_regions_Table reloadData];
        });
        //[self getRegionData];
        
    }] resume];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RegionsTableCell";
    
    RegionsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[RegionsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.region_name.text = [[dataArray[indexPath.row] valueForKey:@"properties"] valueForKey:@"name"];
    cell.otherdata_label.text = @"Description";
    
    if([switchState[indexPath.row]  isEqual: @"off"])
    {
        [cell.switch_cell setOn:NO animated:NO];
    } else {
    [cell.switch_cell setOn:YES animated:NO];
    }
    cell.switch_cell.tag = indexPath.row;
    return cell;
}
/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _main_View;
}*/
- (IBAction)switchAction:(UISwitch *)sender {
    if([sender.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        NSLog(@"table");
        if([sender isOn])
        {
            NSString *idofregion = [NSString stringWithFormat:@"http://faang.woolf.bike/v1/regions/%@",[dataArray[sender.tag] valueForKey:@"id"]];
            
            [self getRegionData:idofregion];
        }
    } else {
        NSLog(@"view");
    }
}
- (IBAction)doneAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Delegate Methods
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectoryPath = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *destinationURL = [NSURL fileURLWithPath:[documentDirectoryPath stringByAppendingPathComponent:@"file.pdf"]];
    
    NSError *error = nil;
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]){
        [fileManager replaceItemAtURL:destinationURL withItemAtURL:destinationURL backupItemName:nil options:NSFileManagerItemReplacementUsingNewMetadataOnly resultingItemURL:nil error:&error];
        //[self showFile:[destinationURL path]];
        
    }else{
        
        if ([fileManager moveItemAtURL:location toURL:destinationURL error:&error]) {
            
            //[self showFile:[destinationURL path]];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PDFDownloader" message:[NSString stringWithFormat:@"An error has occurred when moving the file: %@",[error localizedDescription]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

// 2
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
    /*[self.progressView setProgress:(double)totalBytesWritten/(double)totalBytesExpectedToWrite
                          animated:YES];*/
}

// 3
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PDFDownloader" message:@"Download is resumed successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

//
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
    download = nil;
    //[self.progressView setProgress:0];
    
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PDFDownloader" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}@end
