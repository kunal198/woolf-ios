//
//  SettingsViewController.h
//  Band
//
//  Created by brst on 16/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<NSURLSessionDownloadDelegate>
{
    NSMutableArray *dataArray;
    NSMutableArray *switchState;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_View;
@property (weak, nonatomic) IBOutlet UIView *main_View;
@property (weak, nonatomic) IBOutlet UITableView *regions_Table;
@end
