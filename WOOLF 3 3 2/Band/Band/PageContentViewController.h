//
//  PageContentViewController.h
//  Band
//
//  Created by brst on 20/05/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NGMiBand/NGMiBandViewController.h>

@interface PageContentViewController : NGMiBandViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSInteger device_selectedindex;
}
@property NSUInteger pageIndex;
@property IBOutlet UICollectionView *collectionViewForDevices;
@property id instance;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;
@property (strong, nonatomic) NSMutableArray *devicesArray;
@property (strong, nonatomic) IBOutlet CBPeripheral *connectingperipheral;
@property (strong, nonatomic) IBOutlet UIButton *Pair_status_btn;
@property (strong, nonatomic) IBOutlet UILabel *Battery_Level_label;


@end
