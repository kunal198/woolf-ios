//
//  LoginViewController.m
//  Band
//
//  Created by brst on 07/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import "LoginViewController.h"
#import "WebViewController.h"
#import "SettingsViewController.h"
#import "AppDelegate.h"

@interface LoginViewController ()<UITextFieldDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:187/255 green:185/255 blue:38/255 alpha:0.2].CGColor;
    border.frame = CGRectMake(0, _email_textField.frame.size.height - borderWidth-4, _email_textField.frame.size.width, _email_textField.frame.size.height);
    border.borderWidth = borderWidth;
    [_email_textField.layer addSublayer:border];
    _email_textField.layer.masksToBounds = YES;
    
    _error_Label.text = @"Please enter Email in the Correct Format.";
    _error_Label.hidden = true;
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                            action:@selector(didTapAnywhere:)];
    
    _email_textField.returnKeyType = UIReturnKeyDone;
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [_email_textField resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark - Text Field Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
_error_Label.hidden = true;
}

#pragma mark - Login Action
- (IBAction)loginAction:(UIButton *)sender {
    
    [_email_textField endEditing:true];
    if([self NSStringIsValidEmail:_email_textField.text])
    {
        
        WebViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        newView.isLoginEnabled = _email_textField.text;
        [self.navigationController pushViewController:newView animated:YES];

    } else {
        _error_Label.hidden = false;
    }
}
#pragma mark - Create Action
- (IBAction)createAccountAction:(UIButton *)sender {
    WebViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    newView.isLoginEnabled = @"";
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)settingAction:(UIButton *)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    [self.navigationController pushViewController:add animated:true];
    
    /*NSHTTPCookie *cookie;
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [cookieJar cookies]) {
        //        NSLog(@"%@", [cookie valueForKey:@"name"]);
        //        NSLog(@"%@", [cookie valueForKey:@"value"]);
        //        NSLog(@"%@", [cookie valueForKey:@"domain"]);
        NSLog(@"%@", cookie);
    }*/
}
@end
