//
//  WebViewController.m
//  Band
//
//  Created by brst on 07/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import "WebViewController.h"
#import "MBProgressHUD.h"

@interface WebViewController ()<UIWebViewDelegate>

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%@",_isLoginEnabled);
    if([_isLoginEnabled isEqualToString:@""])
    {
        NSString *urlstr = [NSString stringWithFormat:@"http://faang.woolf.bike/v1/registration?uok=woolf://main"];
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [_web_View loadRequest:requestObj];
    } else {
        
        NSString *urlstr = [NSString stringWithFormat:@"http://faang.woolf.bike/v1/login?uok=woolf://main&usr=%@", _isLoginEnabled];
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        [_web_View loadRequest:requestObj];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Web View Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    //});
    
    //NSLog(@"%@",error);
}
- (BOOL)webView:(UIWebView* )webView shouldStartLoadWithRequest:(NSURLRequest* )request navigationType:(UIWebViewNavigationType)navigationType {
    
    // Determine if we want the system to handle it.
    
    if ([[[request URL] scheme] isEqualToString:@"woolf"]) {
        NSLog(@"%@",[[request URL] scheme]);
        NSLog(@"%@",request);
        NSArray * cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
        NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies: cookies];
        NSString *str = [headers valueForKey:@"Cookie"];
        if([str containsString:@"rememberMe"])
        {
            NSLog(@"login");
            [[NSUserDefaults standardUserDefaults] setObject:headers forKey:@"cookies"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            NSLog(@"create");
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"cookies"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"isloggedin"];
        [webView stopLoading];
        [[UIApplication sharedApplication] openURL:[request URL]];
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    } else {
        return YES;
    }
}
#pragma mark - Button Action

- (IBAction)backAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:true];
    
}
@end
