//
//  WebViewController.h
//  Band
//
//  Created by brst on 07/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *web_View;
@property(nonatomic) NSString* isLoginEnabled;
@end
