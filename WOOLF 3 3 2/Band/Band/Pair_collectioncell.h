//
//  Pair_collectioncell.h
//  Wolf_Application
//
//  Created by Brst981 on 12/05/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Pair_collectioncell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *Pair_img;
@property (strong, nonatomic) IBOutlet UIImageView *pair_selectimg;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *Activityindicator;


@end
