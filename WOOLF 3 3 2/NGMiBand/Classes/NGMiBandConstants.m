//
//  NGMiBandConstants.m
//  Pods
//
//  Created by Simone Camporeale on 09/05/16.
//
//

//#import "NGMiBandConstants.h"

NSString *const IMMEDIATE_ALERT_SERVICE = @"1802";
NSString *const IAS_CHARACTERISTIC = @"2A06";
NSString *const ADVERTISED_SERVICE = @"FEE0";
NSString *const BATTERY_SERVICE = @"0xFF0C";//fee7
NSString *const PAIRING_CHARACTERISTIC = @"FF0F";
NSString *const Battery_CHARACTERISTIC = @"FF0C";

//NSString *const Battery_CHARACTERISTIC = @"2A19";
Byte const PAIRING_CHAR = 0x2;
