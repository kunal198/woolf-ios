//
//  NGMiBand.m
//  Pods
//
//  Created by Simone Camporeale on 09/05/16.
//
//

#import "NGMiBandConstants.h"
#import "NGMiBandViewController.h"

@interface NGMiBandViewController ()
@property (nonatomic,assign) CBCharacteristic* immediateAlertServiceChar;
@property (nonatomic,assign) CBCharacteristic* pairingChar;

@end

@implementation NGMiBandViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *idofconnected = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectedperipheralid"];
    //NSLog(@"%@",idofconnected);
    NSLog(@"PageVisible %i",[[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"]);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"])
    {
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        });
        
        // Do any additional setup after loading the view, typically from a nib.
        self.characteristicDictionary = [[NSMutableDictionary alloc] init];
        _data = [[NSMutableData alloc] init];
        _immediateAlertServiceChar = nil;
    } /*else if ([[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"] == 0 ) {
        
        if (![idofconnected isEqualToString:@""] && [[NSUserDefaults standardUserDefaults] boolForKey:@"fromthirdscreen"] == YES) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"disconnectPeripheral" object:nil];

            static dispatch_once_t once;
            _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
            // Do any additional setup after loading the view, typically from a nib.
            self.characteristicDictionary = [[NSMutableDictionary alloc] init];
            _data = [[NSMutableData alloc] init];
            _immediateAlertServiceChar = nil;
        }
        
    }*/
    //NSLog(@"%i",[[NSUserDefaults standardUserDefaults] integerForKey:@"PageVisible"]);
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_centralManager stopScan];
    if(self.discoveredPeripheral != nil){
        //[self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark :- CentralManager Delegate and Datasource:-
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    // You should test all scenarios
    
    /*if (central.state != CBCentralManagerStatePoweredOn) {
        return;
    }*/
    NSString *stateString = nil;
    switch(central.state)
    {
        case CBCentralManagerStateResetting:
            stateString = @"The connection with the system service was momentarily lost, update imminent.";
            break;
        case CBCentralManagerStateUnsupported:
            stateString = @"The platform doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            stateString = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            stateString = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            stateString = @"Bluetooth is currently powered on and available to use.";
    }
    
    [self scanForPeriphereal:central];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if ( [peripheral.name containsString:@"MI"] && _discoveredPeripheral != peripheral)
    {
        // Save a local copy of the peripheral, so CoreBluetooth doesn't get rid of it
        [[NSNotificationCenter defaultCenter] postNotificationName:@"peripheralFound" object:peripheral];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Failed to connect");
    [self cleanup];
}


- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Connected");
    self.connected = true;
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"connected"];
    [self.centralManager stopScan];
    NSLog(@"Scanning stopped");
    [self.data setLength:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Peripheralconnect" object:peripheral];
    peripheral.delegate = self;
    //  [peripheral discoverServices:nil];
    NSString *serviceAddress = [NSString stringWithFormat:@"%x", MBServiceTypeDefault];
    [peripheral discoverServices:@[[CBUUID UUIDWithString:IMMEDIATE_ALERT_SERVICE], [CBUUID UUIDWithString:ADVERTISED_SERVICE],[CBUUID UUIDWithString:serviceAddress]]];

}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    if (error) {
        [self cleanup];
        return;
    }
    
    for (CBService *service in peripheral.services) {
        self.service = [peripheral.services firstObject];
        [peripheral discoverCharacteristics:nil forService:service];
        
    }
    // Discover other characteristics
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    if (error) {
        [self cleanup];
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        NSString *identifier = [characteristic.UUID UUIDString];
        self.characteristicDictionary[identifier] = characteristic;
        
        self.pairingChar = characteristic;
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:IAS_CHARACTERISTIC]]){
            self.immediateAlertServiceChar = characteristic;
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PAIRING_CHARACTERISTIC]]) {
            self.pairingChar = characteristic;
            NSData *data = [[NSData alloc] initWithBytes:(unsigned char[]){ PAIRING_CHAR } length:1 ];
            [self.discoveredPeripheral writeValue:data forCharacteristic:self.pairingChar type:CBCharacteristicWriteWithResponse];
        }
        else   if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:Battery_CHARACTERISTIC]])
        {
            if (characteristic.value!=NULL)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BatteryLevelFound" object:characteristic.value];
            }
        }
    }
}
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    
    //NSLog(@"%@",error);
    //NSLog(@"%@",characteristic.UUID);
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FF0C"]])
    {
        //NSLog(@"%@",characteristic);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BatteryLevelFound" object:characteristic.value];
        
    }
    
}
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    //NSLog(@"%@",error);
    self.discoveredPeripheral = nil;
    self.connected = false;
    NSString *serviceAddress = [NSString stringWithFormat:@"%x", MBServiceTypeDefault];
    CBUUID *serviceUUID = [CBUUID UUIDWithString:serviceAddress];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"connected"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Peripheraldisconnect" object:peripheral];
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];

    [self.centralManager  scanForPeripheralsWithServices:@[ serviceUUID ]
                                    options:@{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES }];
    
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request
{
}


#pragma  Mark :- Alert Function
- (void)alert:(NGMiBandImmediateAlertLevel)alertLevel {
    if(self.discoveredPeripheral != nil) {
        if(self.immediateAlertServiceChar != nil) {
            short value = [[NSNumber numberWithInt:alertLevel] shortValue] ;
            NSData *data = [NSData dataWithBytes:&value length:1];
            [self.discoveredPeripheral writeValue:data forCharacteristic:self.immediateAlertServiceChar type:CBCharacteristicWriteWithoutResponse];
        }
    }
}


#pragma  Mark :- Battery Function
-(void)readbatery
{
    [self readValueForCharacteristicss:[self characteristicForType:MBCharacteristicTypeBatteryInfo]];
}
- (void)readValueForCharacteristicss:(CBCharacteristic *)characteristic
{
    //NSLog(@"2222222 %@",self.discoveredPeripheral);
    if(characteristic != nil)
    {
    [self.discoveredPeripheral readValueForCharacteristic:characteristic];
    }
}
- (CBCharacteristic *)characteristicForType:(MBCharacteristicType)type {
    NSString *address = [[NSString stringWithFormat:@"%x", (int)type] uppercaseString];
    //NSLog(@"1111111 %@",self.characteristicDictionary);

    return self.characteristicDictionary[address];
}

#pragma connection and Disconnection Methods :-
-(void)peripheral_Disconnection:(CBPeripheral*)peri
{
    //NSLog(@"%@",peri);
    [self.centralManager cancelPeripheralConnection:peri];
}
- (void)cleanup
{
    [self.centralManager cancelPeripheralConnection:_discoveredPeripheral];
}
-(void)peripheral_connection:(CBPeripheral*)peri
{
    [self.centralManager connectPeripheral:peri
                                     options:@{ CBConnectPeripheralOptionNotifyOnConnectionKey: @YES,
                                                CBConnectPeripheralOptionNotifyOnDisconnectionKey: @YES,
                                                CBConnectPeripheralOptionNotifyOnNotificationKey: @YES }];
}

- (bool) connectPeripheral:(CBPeripheral*)peripheral {
    
    //NSLog(@"Discovered %@", peripheral);
    [self.centralManager connectPeripheral:peripheral
                                   options:@{ CBConnectPeripheralOptionNotifyOnConnectionKey: @YES,
                                              CBConnectPeripheralOptionNotifyOnDisconnectionKey: @YES,
                                              CBConnectPeripheralOptionNotifyOnNotificationKey: @YES }];
    return true;
}

#pragma mark :- start and Stop Scanning
-(void)stopscan
{
    [self.centralManager stopScan];
}
- (void)scanForPeriphereal:(CBCentralManager *)central
{
    
    NSString *serviceAddress = [NSString stringWithFormat:@"%x", MBServiceTypeDefault];
    CBUUID *serviceUUID = [CBUUID UUIDWithString:serviceAddress];
    NSArray *connectedPeripherals = [self.centralManager retrieveConnectedPeripheralsWithServices:@[ serviceUUID ]];
    bool peripheralFound = false;
    for (CBPeripheral* peripheral in connectedPeripherals ) {
        
                    if( peripheralFound )
                        break;
                }
    
    
    if (!peripheralFound)
    {
    
    [self.centralManager scanForPeripheralsWithServices:@[ serviceUUID ]
                                                  options:@{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES }];
    

    }
    
}
#pragma Mark :- Helping Method 
- (void)applicationWillTerminate:(UIApplication *)application {
    [_centralManager stopScan];
    if(self.discoveredPeripheral != nil){
        [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
    }
}


@end
