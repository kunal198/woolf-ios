//
//  NGMiBandConstants.h
//  Pods
//
//  Created by Simone Camporeale on 09/05/16.
//
//

#ifndef NGMiBandConstants_h
#define NGMiBandConstants_h

FOUNDATION_EXPORT NSString *const IMMEDIATE_ALERT_SERVICE;
FOUNDATION_EXPORT NSString *const IAS_CHARACTERISTIC;
FOUNDATION_EXPORT NSString *const ADVERTISED_SERVICE;
FOUNDATION_EXPORT NSString *const BATTERY_SERVICE;
FOUNDATION_EXPORT NSString *const PAIRING_CHARACTERISTIC;
FOUNDATION_EXPORT NSString *const Battery_CHARACTERISTIC;
FOUNDATION_EXPORT Byte const PAIRING_CHAR;

#endif /* NGMiBandConstants_h */
