//
//  NgMiBandViewController.h
//  Pods
//
//  Created by Simone Camporeale on 09/05/16.
//
//

#ifndef NgMiBandViewController_h
#define NgMiBandViewController_h

#import <CoreBluetooth/CoreBluetooth.h>


typedef NS_ENUM(int, MBServiceType) {
    MBServiceTypeDefault = 0xFEE0
};

typedef NS_ENUM(NSInteger, MBCharacteristicType) {
    MBCharacteristicTypeDeviceInfo = 0xFF01,
    MBCharacteristicTypeDeviceName,         //0xFF02
    MBCharacteristicTypeNotification,       //0xFF03
    MBCharacteristicTypeUserInfo,           //0xFF04
    MBCharacteristicTypeControl,            //0xFF05
    MBCharacteristicTypeRealtimeSteps,      //0xFF06
    MBCharacteristicTypeActivityData,       //0xFF07
    MBCharacteristicTypeFirmwareData,       //0xFF08
    MBCharacteristicTypeLEParams,           //0xFF09
    MBCharacteristicTypeDateTime,           //0xFF0A
    MBCharacteristicTypeStatistics,         //0xFF0B
    MBCharacteristicTypeBatteryInfo,        //0xFF0C
    MBCharacteristicTypeTest,               //0xFF0D
    MBCharacteristicTypeSensorData          //0xFF0E
};
typedef NS_ENUM(short, NGMiBandImmediateAlertLevel) {
    NGMiBandImmediateAlertLevelNOAlert = 0x00,
    NGMiBandImmediateAlertLevelMid = 0x01,
    NGMiBandImmediateAlertLevelHigh = 0x02,
    NGMiBandImmediateAlertLevelCustom = 0x03,
    NGMiBandImmediateAlertLevelCustom2 = 0x04,
    
};

@interface NGMiBandViewController : UIViewController <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *discoveredPeripheral;
@property (strong, nonatomic) NSMutableData *data;
@property (nonatomic, strong) CBService *service;
@property (nonatomic, strong) NSMutableArray *callbackBlocks;
@property (nonatomic, strong) NSMutableDictionary *characteristicDictionary;
@property (assign, atomic) Boolean connected ;
@property (nonatomic,assign) int pageSelected;
@property (nonatomic, strong) NSMutableArray *ArrayOfPeripherals;
typedef void(^MBPeripheralReadValueResultBlock)(NSData *data, NSError *error);

- (void)alert:(NGMiBandImmediateAlertLevel)alertLevel;
- (void)scanForPeriphereal:(CBCentralManager*)central;
-(void)peripheral_connection:(CBPeripheral*)peri;
-(void)peripheral_Disconnection:(CBPeripheral*)peri;
-(void)readbatery;
-(void)stopscan;

- (NSMutableArray *)arrayCount;

@end
#endif /* NgMiBandViewController_h */
