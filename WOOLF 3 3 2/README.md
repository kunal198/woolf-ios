# NGMiBand

[![CI Status](http://img.shields.io/travis/Simone Camporeale/NGMiBand.svg?style=flat)](https://travis-ci.org/Simone Camporeale/NGMiBand)
[![Version](https://img.shields.io/cocoapods/v/NGMiBand.svg?style=flat)](http://cocoapods.org/pods/NGMiBand)
[![License](https://img.shields.io/cocoapods/l/NGMiBand.svg?style=flat)](http://cocoapods.org/pods/NGMiBand)
[![Platform](https://img.shields.io/cocoapods/p/NGMiBand.svg?style=flat)](http://cocoapods.org/pods/NGMiBand)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NGMiBand is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NGMiBand"
```

## Author

Simone Camporeale, simone.camporeale@netgrid.it

## License

NGMiBand is available under the MIT license. See the LICENSE file for more info.
